using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShaZeslZdelaju
{
    /// <summary>
    /// Main class
    /// </summary>
    public partial class Form1 : Form
    { 
	public Form1()
        {
            InitializeComponent();
        }
    int posBear = 0, posPikachu = 0, posHomjak = 0;
    int turncount = 0;
    double HamsterPow=1, BearPow=1, Pikachupow=1;
    int points1, points2, points;
    Label[] coordinates = new Label[45];
    double hit1, hit2;
    
     /// <summary>
        /// Start button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            Bear.Visible = true;
            Hamster.Visible = true;
            Pikachu.Visible = true;
            RollCube.Visible = true;
            pictureBox4.Visible = true;
            button1.Visible = false;
            label50.Visible = false;
            WHO.Visible = true;

        }
        
         /// <summary>
        /// Ok(after fight) button void
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKbut_Click(object sender, EventArgs e)
        {
            label46.Visible = false;
            label47.Visible = false;
            label48.Visible = false;
            label49.Visible = false;
            fighterCube1.Visible = false;
            fighterCube2.Visible = false;
            OKbut.Visible = false;
            RollCube.Enabled = true;
            HowMuchPtsBack.Visible = false;
            FightResultLabel.Visible = false;
        }
        
        /// <summary>
        /// Exit void
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        
        /// <summary>
        /// Restart void
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        
         /// <summary>
        /// Show game over menu
        /// </summary>
        private void koniecGry()
        {
            button3.Visible = true;
            button2.Visible = true;
            RollCube.Enabled = false;
            RollCube.Visible = false;
            pictureBox4.Visible = false;
            Hamster.Visible = false;
            Bear.Visible = false;
            Pikachu.Visible = false;
            WHO.Visible = false;
        }
        
        /// <summary>
        /// FIGHt method
        /// </summary>
        /// <param name="fighter1"></param>
        /// <param name="fighterstr1"></param>
        /// <param name="position1"></param>
        /// <param name="fighter2"></param>
        /// <param name="fighterstr2"></param>
        /// <param name="position2"></param>
        private void Fight(PictureBox fighter1, double fighterstr1, int position1, PictureBox fighter2, double fighterstr2, int position2)
        {
            label46.Visible = true;
            label46.BringToFront();
            label47.Visible = true;
            label47.BringToFront();
            label48.Visible = true;
            label48.BringToFront();
            label49.Visible = true;
            label49.BringToFront();
            fighterCube1.Visible = true;
            fighterCube1.BringToFront();
            fighterCube2.Visible = true;
            fighterCube2.BringToFront();
            OKbut.Visible = true;
            OKbut.BringToFront();
            RollCube.Enabled = false;
            RollCube.BringToFront();
            HowMuchPtsBack.Visible = true;
            HowMuchPtsBack.BringToFront();
            FightResultLabel.Visible = true;
            FightResultLabel.BringToFront();

            //FIRST PLAYER HIT
            Random randomer1 = new Random();
            points1 = randomer1.Next(1, 7);
            ZminaKuba(points1, fighterCube1);
            label46.Text = fighter1.Name + " hit";
            label48.Text = "DAMAGE DEALT " + (points1 * fighterstr1);
            hit1 = points1 * fighterstr1;

            //SECOND PLAYER HIT
            Random randomer2 = new Random();
            points2 = randomer2.Next(1, 7);
            ZminaKuba(points2, fighterCube2);
            label47.Text = fighter2.Name + " hit";
            label49.Text = "DAMAGE DEALT " + (points2 * fighterstr2);
            hit2 = points2 * fighterstr2;

            //FIRST FIGHTER WIN
            if (hit1 >= hit2)
            {
                FightResultLabel.Text = fighter1.Name + " is the winner!!!";
                if (points1 > points2)
                {
                    HowMuchPtsBack.Text = fighter2.Name + " will be punished back for " + (points1 - points2) + " steps";
                    position2 = position2 - (points1 - points2);
                    if (position2 > 0)
                    {
                        fighter2.Location = coordinates[position2 - 1].Location;
                    }
                    else fighter2.Location = startPOs.Location;
                }
                else
                {
                    HowMuchPtsBack.Text = fighter2.Name + " will be punished back for " + 1 + " step";
                    position2 -= 1;
                    if (position2 > 0)
                    {
                        fighter2.Location = coordinates[position2 - 1].Location;
                    }
                    else fighter2.Location = startPOs.Location;
                }
            }
            //SECOND FIGHTER WIN
            else
            {
                FightResultLabel.Text = fighter2.Name + " is the winner!!!";
                if (points2 > points1)
                {
                    HowMuchPtsBack.Text = fighter1.Name + " will be punished back for " + (points2 - points1) + " steps";
                    position1 = position1 - (points2 - points1);
                    if (position1 > 0)
                    {
                        fighter1.Location = coordinates[position1 - 1].Location;
                    }
                    else fighter1.Location = startPOs.Location;
                }
                else
                {
                    HowMuchPtsBack.Text = fighter2.Name + " will be punished back for " + 1 + " step";
                    position1 -= 1;
                    if (position1 > 0)
                    {
                        fighter1.Location = coordinates[position1 - 1].Location;
                    }
                    else fighter1.Location = startPOs.Location;
                }
            }
        }

        
        /// <summary>
        /// Change cube side
        /// </summary>
        /// <param name="p"></param>
        /// <param name="cube"></param>
        private void ZminaKuba(int p, PictureBox cube)
        {
            if (p == 1)
            {
                cube.Image = Properties.Resources.с1;
            }
            if (p == 2)
            {
                cube.Image = Properties.Resources.с2;
            }
            if (p == 3)
            {
                cube.Image = Properties.Resources.с3;
            }
            if (p == 4)
            {
                cube.Image = Properties.Resources.с4;
            }
            if (p == 5)
            {
                cube.Image = Properties.Resources.с5;
            }
            if (p == 6)
            {
                cube.Image = Properties.Resources.с6;
            }
        }
    
        /// <summary>
        /// Roll the cube
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            turncount++;

            Random randomer = new Random();
            points = randomer.Next(1, 7);
            ZminaKuba(points, pictureBox4);

            //TURN BEAR
            if (turncount == 1)
            {
                if (posBear + points >= 45)
                {
                    PlayerWinLabel.Text = "BEAR WIN";
                    Bear.Location = label45.Location;
                    PlayerWinLabel.Visible = true;
                    koniecGry();
                }
                else
                {
                    posBear += points;
                    BearPow = BearPow + (Convert.ToDouble(coordinates[posBear-1].Text));
                    bearPowLablel.Text = "Bear power: " + BearPow;
                    Bear.Location = coordinates[posBear - 1].Location;
                    if (posBear == posHomjak)
                    {
                        Fight(Bear, BearPow, posBear, Hamster, HamsterPow, posHomjak);
                    }
                    else if (posBear == posPikachu)
                    {
                        Fight(Bear, BearPow, posBear, Pikachu, Pikachupow, posPikachu);
                    }
                }
                WHO.Text = "Pikachu turn";
            }

            //TURN PIKACHU
            else if (turncount == 2)
            {
                if (posPikachu + points >= 45)
                {
                    PlayerWinLabel.Text = "PIKACHU WIN";
                    Pikachu.Location = label45.Location;
                    PlayerWinLabel.Visible = true;
                    koniecGry();
                }
                else
                {
                    posPikachu += points;
                    Pikachupow = Pikachupow + (Convert.ToDouble(coordinates[posPikachu - 1].Text));
                    PikPowLabel.Text = "Pikachu power: " + Pikachupow;
                    Pikachu.Location = coordinates[posPikachu - 1].Location;
                    if (posPikachu == posHomjak)
                    {
                        Fight(Pikachu, Pikachupow, posPikachu, Hamster, HamsterPow, posHomjak);
                    }
                    else if (posPikachu == posBear)
                    {
                        Fight(Pikachu, Pikachupow, posPikachu, Bear, BearPow, posBear);
                    }
                }
                WHO.Text = "Hamster turn";
            }

            //TURN HAMSTER
            else if (turncount == 3)
            {
                if (posHomjak + points >= 45)
                {
                    PlayerWinLabel.Text = "HAMSTER WIN";
                    Hamster.Location = label45.Location;
                    PlayerWinLabel.Visible = true;
                    koniecGry();
                }
                else
                {
                    posHomjak += points;
                    HamsterPow = HamsterPow + (Convert.ToDouble(coordinates[posHomjak - 1].Text));
                    HamPowLabel.Text = "Hamster power: " + HamsterPow;
                    Hamster.Location = coordinates[posHomjak - 1].Location;
                    turncount = 0;
                    if (posHomjak == posBear)
                    {
                        Fight(Hamster, HamsterPow, posHomjak, Bear, BearPow, posBear);
                    }
                    else if (posHomjak == posPikachu)
                    {
                        Fight(Hamster, HamsterPow, posHomjak, Pikachu, Pikachupow, posPikachu);
                    }
                }
                WHO.Text = "Bear turn";
            }
        }
 /// <summary>
        /// Load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {         
            Bear.BackColor = Color.FromArgb(0, 0, 0, 0);
            Hamster.BackColor = Color.FromArgb(0, 0, 0, 0);
            Pikachu.BackColor = Color.FromArgb(0, 0, 0, 0);
            coordinates[0] = label1;
            coordinates[1] = label2;
            coordinates[2] = label3;
            coordinates[3] = label4;
            coordinates[4] = label5;
            coordinates[5] = label6;
            coordinates[6] = label7;
            coordinates[7] = label8;
            coordinates[8] = label9;
            coordinates[9] = label10;
            coordinates[10] = label11;
            coordinates[11] = label12;
            coordinates[12] = label13;
            coordinates[13] = label14;
            coordinates[14] = label15;
            coordinates[15] = label16;
            coordinates[16] = label17;
            coordinates[17] = label18;
            coordinates[18] = label19;
            coordinates[19] = label20;
            coordinates[20] = label21;
            coordinates[21] = label22;
            coordinates[22] = label23;
            coordinates[23] = label24;
            coordinates[24] = label25;
            coordinates[25] = label26;
            coordinates[26] = label27;
            coordinates[27] = label28;
            coordinates[28] = label29;
            coordinates[29] = label30;
            coordinates[30] = label31;
            coordinates[31] = label32;
            coordinates[32] = label33;
            coordinates[33] = label34;
            coordinates[34] = label35;
            coordinates[35] = label36;
            coordinates[36] = label37;
            coordinates[37] = label38;
            coordinates[38] = label39;
            coordinates[39] = label40;
            coordinates[40] = label41;
            coordinates[41] = label42;
            coordinates[42] = label43;
            coordinates[43] = label44;
            coordinates[44] = label45;

        }
	}
}